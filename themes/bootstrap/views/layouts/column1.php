<?php $this->beginContent('//layouts/main'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
		    <div class="panel-heading">
		        <h3 class="panel-title">
		        	<?php
                        echo BsHtml::pageHeader($this->pageCaption, $this->pageDescription);
                    ?>
		        </h3>
		    </div>
			<div class="panel-body">
				<?php echo $content; ?>
		    </div>
		</div>
	</div>
</div>
<?php $this->endContent(); ?>