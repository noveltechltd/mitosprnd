<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->pageCaption=$this->pageTitle;
$this->breadcrumbs=array(
    'Login',
);
?>


<p class="alert">Please fill out the following form with your login credentials:</p>

<div class="form">
<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'login-form',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php
       echo $form->textFieldControlGroup($model, 'username', array(
           'labelOptions' => array('class' => 'col-md-2 col-lg-2'),
           'controlOptions' => array('class' => 'col-md-6 col-lg-6')
       ));
  ?>

   <?php
   echo $form->passwordFieldControlGroup($model, 'password', array(
       'labelOptions' => array('class' => 'col-md-2 col-lg-2'),
       'controlOptions' => array('class' => 'col-md-6 col-lg-6')
   ));
   ?>

	<div class="rememberMe">
       <?php
       echo $form->checkBoxControlGroup($model, 'rememberMe', array(
           'labelOptions' => array('class' => 'col-md-2 col-lg-2'),
           'controlOptions' => array('class' => 'col-md-8 col-lg-8')
       ));
       ?>
   </div>

	<div class="row buttons">
		<?php
            echo BsHtml::formActions(array(BsHtml::submitButton('Login')));
        ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
