<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle=Yii::app()->name . ' - Contact Us';
$this->pageCaption=$this->pageTitle;
$this->breadcrumbs=array(
    'Contact',
);
?>

<?php if (Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>

<p class="alert">
If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.
</p>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'contact-form',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

		<?php
           echo $form->textFieldControlGroup($model, 'name', array(
               'labelOptions' => array('class' => 'col-md-2 col-lg-2'),
               'controlOptions' => array('class' => 'col-md-6 col-lg-6')
           ));
      ?>

		<?php
           echo $form->textFieldControlGroup($model, 'email', array(
               'labelOptions' => array('class' => 'col-md-2 col-lg-2'),
               'controlOptions' => array('class' => 'col-md-6 col-lg-6')
           ));
      ?>

		<?php
           echo $form->textFieldControlGroup($model, 'subject', array(
               'labelOptions' => array('class' => 'col-md-2 col-lg-2'),
               'controlOptions' => array('class' => 'col-md-6 col-lg-6')
           ));
      ?>

		<?php
           echo $form->textAreaControlGroup($model, 'body', array(
               'labelOptions' => array('class' => 'col-md-2 col-lg-2'),
               'controlOptions' => array('class' => 'col-md-6 col-lg-6')
           ));
      ?>


	<?php if (CCaptcha::checkRequirements()): ?>

		<div class="form-group">
			<label class="control-label col-md-2 col-lg-2 required"><?php echo $form->labelEx($model, 'verifyCode'); ?> </label>
			<div class="col-md-6 col-lg-6">
				<?php $this->widget('CCaptcha'); ?>
				<?php echo $form->textField($model, 'verifyCode'); ?>
				<p id="ContactForm_body_em_"  class="help-block">
					Please enter the letters as they are shown in the image above.
							<br/>Letters are not case-sensitive.
					<p style="color:red;"><?php echo $form->error($model, 'verifyCode'); ?></p>
				</p>
			</div>
		</div>
	<?php endif; ?>

	<div class="row buttons">
		<?php
            echo BsHtml::formActions(array(BsHtml::submitButton('Submit')));
        ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php endif; ?>